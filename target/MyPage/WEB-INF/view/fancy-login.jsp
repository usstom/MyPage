<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Resume</title>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/green.css" />
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/print.css"
	media="print" />
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/login.css">
<!--[if IE 7]>
<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if IE 6]>
<link href="css/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.tipsy.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/cufon.yui.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/scrollTo.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/myriad.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.colorbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
<script type="text/javascript">
	Cufon.replace('h2');
</script>
<!-- Reference Bootstrap files -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.mins.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<!--  old head
<head>

<title>Login Page</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>-->

<body>
	<!-- Begin Wrapper -->
	<div id="wrapper">
		<div class="wrapper-top"></div>
		<div class="wrapper-mid">
			<!-- Begin Paper -->
			<div id="paper">
				<div class="paper-top"></div>
				<div id="paper-mid">
					<div class="entry">
						<!-- Begin Image -->
						<img class="portrait"
							src="${pageContext.request.contextPath}/resources/images/avatar.jpg"
							alt="Tam�s Heged�s" />
						<!-- End Image -->
						<!-- Begin Personal Information -->
						<div class="self">
							<h1 class="name">
								Tam�s Heged�s <br /> <span>Java Developer</span>
							</h1>
							<ul>
								<li class="ad">Budapest 1143 Z�szl�s Street 31</li>
								<li class="mail">tamas.hegedus.87@gmail.com</li>
								<li class="tel">+36 30 360 6447</li>
								<li class="web">tamashegedus.cloud   gitlab.com/usstom</li>
							</ul>
						</div>
						<!-- End Personal Information -->
						<!-- Begin Social -->
						<div class="social">
							<ul>
								<li><a class='north' href="#" title="Download .pdf"><img
										src="${pageContext.request.contextPath}/resources/images/icn-save.jpg"
										alt="Download the pdf version" /></a></li>
								<li><a class='north' href="javascript:window.print()"
									title="Print"><img
										src="${pageContext.request.contextPath}/resources/images/icn-print.jpg"
										alt="" /></a></li>
							</ul>
						</div>
						<!-- End Social -->
					</div>
					<!-- Begin 1st Row -->
					<!--<div class="entry">
						<h2>OBJECTIVE</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Proin dignissim viverra nibh sed varius. Proin bibendum nunc in
							sem ultrices posuere. Aliquam ut aliquam lacus.</p>
					</div>-->
					<!-- End 1st Row -->
					<!-- Begin 2nd Row -->
					<div class="entry">
						<h2>STUDIES</h2>
						<div class="content">
							<h4>
								Braining HUB <br /> Training Center <br /> 2017.03-07
							</h4>
							<p>
								Junior JAVA programmer training <br /> <em>Four month
									intensive programmer training. Learn the basics : arrays,
									collections, programming theorems, methods, classes, objects,
									interfaces. Object oriented software development skills. Java
									EE technological skills. Practice-oriented training focusing on
									teamwork, version tracking and learning the Scrum methodology.
								</em>
							</p>
						</div>
						<div class="content">
							<h4>
								�budai University <br /> Budapest <br /> 2006-2013
							</h4>
							<p>
								Kand� K�lm�n Faculty of Electrical Engineering <br /> <em>Institute
									of Communication Engineering</em>
							</p>
						</div>
						<div class="content">
							<h4>
								Pataky Istv�n <br /> High School <br /> 2002-2006
							</h4>
							<p>
								School of Telecommunications and Information Technology <br />
								<em> </em>
							</p>
						</div>
					</div>
					<!-- End 2nd Row -->
					<!-- Begin 3rd Row -->
					<div class="entry">
						<h2>EXPERIENCE</h2>
						<div class="content">
							<h4>
								BrainingHUB <br /> 2017.03-07
							</h4>
							<p>
								Junior JAVA Developer <br /> <em>Java client software
									development featuring real-time connectivity with dataservers
									and versatile data processing capability. Designing,
									implementing and testing algorithm-controlled console
									applications. Modelling object oriented tasks using MVC
									architectural pattern. Designing, implementing and testing Java
									swing applications. Familiar with NetBeans development tool.</em>
							</p>
						</div>
						<div class="content">
							<h4>
								M�V Zrt. <br /> 2015.04-
							</h4>
							<p>
								Technological System Engineer <br /> <em> </em>
							</p>
							<ul class="info">
								<li>Operation and supervision of different types of
									telecommunication equipment operating under the Regional
									Directorate</li>
								<li>Participation in laboratory and on site test for GSM-R
									related equipment</li>
							</ul>
						</div>
						<div class="content">
							<h4>
								TR-Soft 2000 Kft. <br /> 2014.07-10
							</h4>
							<p>
								Software Operator <br /> <em> </em>
							</p>
							<ul class="info">
								<li>Communication with clients</li>
								<li>Testing Software Versions</li>
								<li>Recording and handling software errors</li>
							</ul>
						</div>
					</div>
					<!-- End 3rd Row -->
					<!-- Begin 4th Row -->
					<div class="entry">
						<h2>SKILLS</h2>
						<div class="content">
							<h3>Languages</h3>
							<ul class="skills">
								<li>Java</li>
								<li>HTML</li>
								<li>XML</li>
								<li>JSON</li>
								<li>SQL</li>
								<li>JavaEE</li>
								<li>Spring</li>
								<li>Maven</li>
								<li>Git</li>

							</ul>
						</div>
						<div class="content">
							<h3>Environments</h3>
							<ul class="skills">
								<li>Photoshop</li>
								<li>Windows</li>
								<li>Linux</li>
								<li>Android</li>
								<li>Tomcat</li>
								<li>Payara(GlassFish)</li>
							</ul>
						</div>
						<div class="content">
							<h3>Language</h3>
							<ul class="skills">
								<li>English : intermediate level (B2+)</li>

							</ul>
						</div>
						<div class="content">
							<h3>Others</h3>
							<ul class="skills">
								<li>Driving license B</li>
								<li>Strongly motived</li>
								<li>Good team player</li>
								<li>Interest in innovations</li>
								<li>Precision in work</li>
							</ul>
						</div>
					</div>
					<!-- End 4th Row -->
				</div>
				<div class="clear"></div>
				<div class="paper-bottom"></div>
			</div>
			<!-- End Paper -->
		</div>
		<div class="wrapper-bottom"></div>
	</div>
	<div id="message">
		<a href="#top" id="top-link">Go to Top</a>
	</div>
	<!-- End Wrapper -->
</body>
</html>